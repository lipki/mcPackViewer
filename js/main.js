"use strict";

var $ = require('jquery');




var THREE = require('three');
var OrbitControls = require('three-orbit-controls')(THREE);
var OBJLoader = require('three-obj-loader')(THREE);


var camera, scene, renderer, data, texture, material;

var url = "rp4/entity/steve.obj";


new THREE.TextureLoader().load(
	"assets/lipki-SummerFields-dev/assets/minecraft/textures/entity/steve.png",
	function ( data ) {

		console.log(data);
		var ctx = document.createElement('canvas').getContext('2d');
		ctx.canvas.height = data.image.height;
		ctx.canvas.width = data.image.width;
		document.body.appendChild( ctx.canvas );
		ctx.rect(0,0,data.image.width,data.image.height);
		ctx.fillStyle = "#00000001";
		ctx.fill();
		ctx.drawImage(data.image,0,0);

		texture = new THREE.Texture(ctx.canvas);
		texture.magFilter = THREE.NearestFilter;
		texture.minFilter = THREE.NearestFilter;
		console.log(texture);

		material = new THREE.MeshBasicMaterial({
			map:texture,
			transparent:true,
			side: THREE.DoubleSide,
			opacity: 0.5
		});


		init();
		animate();

	}
);



function init() {

	// renderer

	renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	//renderer.sortObjects = false;
	renderer.setClearColor( 0xffffff, 0);

	document.body.appendChild( renderer.domElement );

	// scene

	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xffffff );

	// camera

	//camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
	//camera.position.z = 50;
	//camera.lookAt(new THREE.Vector3());

	var frustumSize = 5;
	var aspect = window.innerWidth / window.innerHeight;
	camera = new THREE.OrthographicCamera( frustumSize * aspect / - 2, frustumSize * aspect / 2, frustumSize / 2, frustumSize / - 2, 1, 2000 );
	camera.position.x = 16;
	camera.position.y = 16;
	camera.position.z = 32;
	camera.lookAt(new THREE.Vector3());

	// controls

	var controls = new OrbitControls(camera);
  controls.enableDamping = true
  controls.dampingFactor = 0.2
  controls.zoomSpeed = 1.4
  controls.rotateSpeed = 0.6
	controls.enableKeys = false

  // lights

  var light

  light = new THREE.AmbientLight(0xffffff, 0.97);
  scene.add(light);

  light = new THREE.DirectionalLight(0xffffff, 0.1);
  light.position.set(4, 10, 6);
	scene.add(light);

	// instantiate a loader
	var loader = new THREE.OBJLoader();

	// load a resource
	loader.load( url,
		function ( object ) {
			object.children[0].material = material;
			scene.add( object );
		},
		function ( xhr ) {
			console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
		},
		function ( error ) {
			console.log( 'An error happened' );
		}
	);

	//scene.add( new THREE.Mesh( new THREE.SphereGeometry( 1, 1, 1 ), new THREE.MeshBasicMaterial() ) );

	window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}

function uvmap(geo, k, u0, v0, u1, v1, size) {
	// 01-left _ 23-right _ 45-top _ 67-bottom _ 89-front _ 1011-back
	var i;

  var x0 = u0/size.x;
  var x1 = u1/size.x;
  var y0 = (size.y-v0)/size.y;
  var y1 = (size.y-v1)/size.y;

  geo.faceVertexUvs[0][k] = [
      new THREE.Vector2(	x0, y1  ),		// TL
      new THREE.Vector2(	x0, y0  ),		// BL
      new THREE.Vector2(	x1, y1  )		  // TR
  ];
  geo.faceVertexUvs[0][k+1] = [
      new THREE.Vector2(	x0, y0  ),		// BL
      new THREE.Vector2(	x1, y0  ),		// BR
      new THREE.Vector2(	x1, y1  )		  // TR
  ];
}
